var Controller = angular.module('Controller', []);

Controller.controller('Controller', ['$scope', '$http',
    function ($scope, $http) {
        var totalWishes = 0;
        var totalBag = 0;
        var total = 0;
        var prices = [];

        $('.item--wished').on('click', function(){
            var $this = $(this);
            var hasClass = $this.hasClass('item--wished__favorite');
            $this.toggleClass('item--wished__favorite');

            if(!hasClass){
                totalWishes++;
                $('.total-wished').html(totalWishes);

            }else if(hasClass){
                totalWishes--;
                $('.total-wished').html(totalWishes);
            }
        });

        $('.button').on('click', function(e){
            var $this = $(this);
            var price = Number($this.attr('data-price'));
            var savedPrice = price;
            var sum = 0;
            var total = null;
            
            prices.push(price);

            $this.toggleClass('in-cart');

            if($this.hasClass('in-cart')){
                $this.html('In cart');
                totalBag++;

            }else{
                $this.html('Add to cart');
                $this.attr('data-price', savedPrice);
                totalBag--;

                for(var i=0; i < prices.length; i++ ){ 
                    if(prices[i] === savedPrice){
                       prices.splice(i, 1);
                       i--;
                   }
               }
            }

            $('.total-bag').html(totalBag);
            
            prices.forEach(function(num){
                sum += parseFloat(num) || 0;

                total = '€ ' + sum;
            });

            $('.total-cart').html(total);
            
            e.preventDefault();
        });

    }]
);
