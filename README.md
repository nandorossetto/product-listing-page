# Product List Page

To run the app you need to have installed the stack bellow:

 * Nodejs
 * Ruby / Compass
 * GruntJS


If you don't, visit these websites to help you

 * https://nodejs.org/en/
 * https://www.ruby-lang.org/
 * https://gruntjs.com/

After that, run npm install into the root folder. Then, run grunt serve in your terminal and open your browser using the port: 9001 (http://127.0.0.1:9001).

To prepare your app to deploy, run grunt deploy. This task will minify, uglify and merge the files to be ready to deploy.

this app is using: https://github.com/nandorossetto/katrina-bootstrap-webapp